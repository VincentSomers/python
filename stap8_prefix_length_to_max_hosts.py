#https://github.com/sumitmcc/subnetcalc/blob/master/subnetcalc.py
def prefix_length_to_max_hosts (netmask):
    net = netmask#.split(".")
    net_bin = [format(int(i), '08b') for i in net]

    string = [str(integer) for integer in net_bin]
    a_string = "".join(string)

    zeros = a_string.count("0")
    ones = 32 - zeros
    hosts = abs(2 ** zeros - 2)
    print (hosts)
