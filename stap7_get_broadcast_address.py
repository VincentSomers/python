def get_broadcast_address(network_address, wildcard_mask):
    ip = network_address#.split(".")
    net = wildcard_mask#.split(".")

    ip_1 = int(ip[0])
    ip_2 = int(ip[1])
    ip_3 = int(ip[2])
    ip_4 = int(ip[3])

    net_1 = int(net[0])
    net_2 = int(net[1])
    net_3 = int(net[2])
    net_4 = int(net[3])

    #result1 = f"{ip_1 | net_1}.{ip_2 | net_2}.{ip_3 | net_3}.{ip_4 | net_4}"
    result1 = [f"{ip_1 | net_1},{ip_2 | net_2},{ip_3 | net_3},{ip_4 | net_4}"]
    print (result1)
