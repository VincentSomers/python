def one_bits_in_netmask(counterlist):
    octet = counterlist#.split(".")
    octet_bin = [format(int(i), '08b') for i in octet]
    binary_netmask = ("").join(octet_bin)
    #print(binary_netmask)
    counter = 0
    for symbol in binary_netmask:
	    if symbol == "1":
		    counter +=1
    return counter
