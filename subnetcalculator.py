import sys

def ask_for_number_sequence1(message):
	print(message)
	ip=input()
	return[int(elem) for elem in ip.split(".")]


def ask_for_number_sequence2(message):
	print(message)
	ip=input()
	return[int(elem) for elem in ip.split(".")]


def is_valid_ip_address(numberlist):
	ips = numberlist
	result = True
	for ip in ips:
		num = int(ip)
		if not (num >= 0 and num <= 255):
			result = False
	if len(ips) != 4:
		print("IP-adres is niet geldig!")
		return False
		sys.exit(0)

	if result == False:
		print("IP-adres is niet geldig!")
		sys.exit(0)
	else:
		print("IP-adres is geldig!")


def is_valid_netmask(numberlist):
	ips = numberlist
	result = True
	for ip in ips:
		num = int(ip)
		if not (num >= 0 and num <= 255):
			result = False
	if len(ips) != 4:
                print("Netmask is niet geldig!")
                return False
                sys.exit(0)

	if result == False:
		print("Netmask is niet geldig!")
		sys.exit(0)
	else:
		print("Netmask is geldig")
		octet = ips
		octet_bin = [format(int(i), '08b') for i in octet]
		binary_netmask = ("").join(octet_bin)
		#print(binary_netmask)

		checking_ones = True
		for symbol in binary_netmask:
			if checking_ones and symbol == "0":
				return False
			elif symbol == "1":
				checking_ones = True
		return True


def one_bits_in_netmask(counterlist):
    octet = counterlist#.split(".")
    octet_bin = [format(int(i), '08b') for i in octet]
    binary_netmask = ("").join(octet_bin)
    #print(binary_netmask)
    counter = 0
    for symbol in binary_netmask:
	    if symbol == "1":
		    counter +=1
    print("De lengte van het subnetmasker is", counter)


def apply_network_mask(host_address, netmask):
    ip = host_address#.split(".")
    net = netmask#.split(".")

    ip_1 = int(ip[0])
    ip_2 = int(ip[1])
    ip_3 = int(ip[2])
    ip_4 = int(ip[3])

    net_1 = int(net[0])
    net_2 = int(net[1])
    net_3 = int(net[2])
    net_4 = int(net[3])

    result1 = f"{ip_1 & net_1}.{ip_2 & net_2}.{ip_3 & net_3}.{ip_4 & net_4}"
    #result1 = [f"{ip_1 & net_1},{ip_2 & net_2},{ip_3 & net_3},{ip_4 & net_4}"]
    #result2 = result1.replace(" ",".")
    print ("Het adres van het subnet is", result1)


def netmask_to_wilcard_mask(netmask):
    wildcard_mask = []
    for x in netmask:
        component = 255 - int(x)
        wildcard_mask.append(str(component))
    wildcard = ','.join(wildcard_mask)
    wild = wildcard.replace(',','.')
    print("Het wildcardmasker is", wild)
    return wildcard_mask


def get_broadcast_address(network_address, wildcard_mask):
    ip = network_address#.split(".")
    net = wildcard_mask#.split(".")

    ip_1 = int(ip[0])
    ip_2 = int(ip[1])
    ip_3 = int(ip[2])
    ip_4 = int(ip[3])

    net_1 = int(net[0])
    net_2 = int(net[1])
    net_3 = int(net[2])
    net_4 = int(net[3])

    result1 = f"{ip_1 | net_1}.{ip_2 | net_2}.{ip_3 | net_3}.{ip_4 | net_4}"
    #result1 = [f"{ip_1 | net_1},{ip_2 | net_2},{ip_3 | net_3},{ip_4 | net_4}"]
    print("Het broadcastadres is", result1)
    return result1


def prefix_length_to_max_hosts (netmask):
    net = netmask#.split(".")
    net_bin = [format(int(i), '08b') for i in net]

    string = [str(integer) for integer in net_bin]
    a_string = "".join(string)

    zeros = a_string.count("0")
    ones = 32 - zeros
    hosts = abs(2 ** zeros - 2)
    print ("Het maximaal aantal hosts op dit subnet is", hosts)


if __name__ == '__main__':
    host_adres = ask_for_number_sequence1("Wat is het IP-adres?\n")
    subnet_mask = ask_for_number_sequence2("Wat is het subnetmasker?\n")
    valid_ip = is_valid_ip_address(host_adres)
    valid_subnet = is_valid_netmask(subnet_mask)
    prefix = one_bits_in_netmask(subnet_mask)
    net_mask = apply_network_mask(host_adres, subnet_mask)
    wildcard = netmask_to_wilcard_mask(subnet_mask)
    broadcast = get_broadcast_address(host_adres, wildcard)
    max_prefix = prefix_length_to_max_hosts(subnet_mask)
