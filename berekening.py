def apply_network_mask(host_address, netmask):
    ips = host_address.split(".")
    net = netmask.split(".")

    ips_bin = [format(int(i), '08b') for i in ips]

    net_bin = [format(int(i), '08b') for i in net]

    ips_list = [ips_bin]
    net_list = [net_bin]

    inverted =""
    for symbol in ips_list and net_list:
        if symbol == "0":
            inverted += "1"
        if symbol == "1":
            inverted += "0"
    print(inverted)
